
# PhotoGateway

Aplicația constă în două proiecte:

**1. Backend**: https://gitlab.upt.ro/vlad.bagacian/photogateway.be

**2. Frontend**: https://gitlab.upt.ro/vlad.bagacian/photogateway.fe

Pentru rularea întregii aplicații ( BE+FE ) este nevoie să se ruleze întâi aplicația backend, iar apoi aplicația frontend. 
Având în vedere ca aplicația folosește Auth0 ca provider de autentificare, pentru funcționarea aplicației în contextul utilizatoriilor autentificați, este necesară o conexiune la internet.


## 1. Backend

Dependențe necesare:

 - [ ] .NET 5.0 https://dotnet.microsoft.com/download/dotnet/5.0
 - [ ] MongoDB 6.0.4 https://www.mongodb.com/try/download/community
 - [ ] (Opțional) Visual Studio 2022 https://visualstudio.microsoft.com/vs/

Pași rulare aplicație:
1. (Opțional): În mod implicit conexiunea cu MongoDB, Auth0 și SendinBlue se face automat. Pentru a modifica setăriile necesare conexiunii, se pot schimba în fișierul ***appsettings.json*** valorile următoarelor chei: 
```
Auth0Settings, MongoDbSettings, EmailConfiguration
```
2. Deschidere command line interface
```
cd directory
dotnet restore
dotnet build
dotnet run
```
3. Verificare rulare aplicație prin accesarea [Swagger UI](https://localhost:5001/swagger/index.html).
## 2. Frontend

Dependențe necesare:

 - [ ] NodeJS [https://nodejs.org/](https://nodejs.org)
 - [ ] (Opțional) Visual Studio Code https://code.visualstudio.com/

Pași rulare aplicație:
 1. (Opțional): Dacă s-au modificat setăriile din aplicația backend, cum ar fi port-ul, sau datele de la Auth0, este necesar să se modifice constantele din fișierul ***.env***
```
Auth0Settings, MongoDbSettings, EmailConfiguration
```
 2. Deschidere command line interface
```
cd directory
npm install
npm run build
npm run dev
```
 3. Dacă aplicația va rula sub un alt port decât cel preconfigurat ( 5173 ), este necesar a se schimba în fișierul ***package.json*** sub nodul ***scripts/dev*** cu urmatoarea valoare: "vite --port 5173".
 4. Verificare rulare aplicație prin accesarea [acestui](http://127.0.0.1:5173/) URL.
