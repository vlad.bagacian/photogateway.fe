import Address from './Address'

class CreateUserRequest {
  email: string
  password: string
  firstName: string
  lastName: string
  role: string
  phone: string
  address: Address
  constructor(data?: any) {
    this.email = data?.email
    this.password = data?.password
    this.firstName = data?.firstName
    this.lastName = data?.lastName
    this.role = data?.role
    this.phone = data?.phone
    this.address = data?.address ?? new Address()
  }
}

export default CreateUserRequest
