import Session from './Session'
import { SessionStatus } from '../constants/Common'
import ListResponse from './ListResponse'
import Photography from './Photography'
export class SessionState {
  session?: Session
  status: SessionStatus
  nextButtonTitle: string
  backButtonTitle: string
  isPhotographer: boolean
  canMoveBack: boolean
  canMoveNext: boolean
  canEdit: boolean
  disableNext: boolean
  isSessionEdited: boolean
  showUpload: boolean
  photos: ListResponse<Photography>
  showGallery: boolean
  selectedCount: number
  displaySelectedProgress: boolean
  disablePhotoSelection: boolean
  canLeaveReview: boolean
  constructor(session?: Session, role?: string) {
    this.status = session?.status ?? SessionStatus.None
    this.session = session
    this.isPhotographer = role === 'Photographer'
    this.disableNext = false
    this.isSessionEdited = false
    this.nextButtonTitle = ''
    this.backButtonTitle = ''
    this.canMoveBack = false
    this.canMoveNext = false
    this.canEdit = false
    this.showUpload = false
    this.photos = new ListResponse<Photography>()
    this.showGallery = false
    this.selectedCount = 0
    this.displaySelectedProgress = false
    this.disablePhotoSelection = false
    this.canLeaveReview = false
    switch (this.status) {
      case SessionStatus.New:
        this.canMoveBack = true
        this.canMoveNext = this.isPhotographer
        this.canEdit = false
        this.nextButtonTitle = 'Accept'
        this.backButtonTitle = 'Cancel'
        break
      case SessionStatus.ProjectReview:
        this.canMoveBack = !this.isPhotographer
        this.canMoveNext = this.isPhotographer
        this.canEdit = this.isPhotographer
        this.nextButtonTitle = 'Send quote'
        this.backButtonTitle = 'Cancel'
        this.disableNext = session !== undefined && session.cost >= 0
        break
      case SessionStatus.Approval:
        this.canMoveBack = !this.isPhotographer
        this.canMoveNext = !this.isPhotographer
        this.canEdit = false
        this.nextButtonTitle = 'Accept'
        this.backButtonTitle = 'Cancel'
        break
      case SessionStatus.InProgress:
        this.canMoveBack = false
        this.canMoveNext = this.isPhotographer
        this.canEdit = false
        this.nextButtonTitle = 'Send photos'
        this.backButtonTitle = ''
        this.showUpload = this.isPhotographer
        break
      case SessionStatus.InReview:
        this.canMoveBack = false
        this.canMoveNext = !this.isPhotographer && this.selectedCount > 0
        this.canEdit = false
        this.nextButtonTitle = 'Accept photos'
        this.backButtonTitle = ''
        this.showGallery = true
        this.displaySelectedProgress = !this.isPhotographer
        this.disablePhotoSelection = this.isPhotographer
        break
      case SessionStatus.Completed:
        this.canMoveBack = false
        this.canMoveNext = true
        this.canEdit = false
        this.nextButtonTitle = 'Download photos'
        this.backButtonTitle = ''
        this.showGallery = true
        this.disablePhotoSelection = true
        this.canLeaveReview = !this.isPhotographer
        break
      case SessionStatus.Cancelled:
        this.canMoveBack = false
        this.canMoveNext = false
        this.canEdit = false
        this.nextButtonTitle = ''
        this.backButtonTitle = ''
        break
    }
  }

  setPhotos(photos: ListResponse<Photography>) {
    if (this.status == SessionStatus.Completed) {
      this.photos.items = photos.items.filter((photo) => photo.selected)
      this.photos.count = this.photos.items.length
    } else {
      this.photos = photos
    }

    this.showGallery = photos.count > 0
    this.selectedCount = photos.items.filter((photo) => photo.selected).length
  }

  updatePhoto(photo: Photography) {
    const photoIndex = this.photos.items.findIndex((p) => p.id === photo.id)
    if (photoIndex >= 0) {
      this.photos.items[photoIndex] = photo
    }

    this.selectedCount = this.photos.items.filter(
      (photo) => photo.selected
    ).length
  }

  setCanMoveNext() {
    this.canMoveNext =
      this.selectedCount > 0 &&
      !this.isPhotographer &&
      this.status == SessionStatus.InReview
  }
}

export default SessionState
