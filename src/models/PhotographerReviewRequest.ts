class PhotographerReviewRequest {
  comment: string
  rating: number
  constructor(data?: any) {
    this.comment = data?.comment
    this.rating = data?.number ?? 0
  }
}

export default PhotographerReviewRequest
