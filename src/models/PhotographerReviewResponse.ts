class PhotographerReviewResponse {
  comment: string
  rating: number
  createdAt: string
  clientName: string
  constructor(data?: any) {
    this.comment = data?.comment
    this.rating = data?.number ?? 0
    this.clientName = data?.clientName
    this.createdAt = data?.createdAt
  }
}

export default PhotographerReviewResponse
