class PhotoReview {
  imageUrl: string
  comment: string
  createdAt: string
  userName: string
  sent: boolean
  constructor(data?: any) {
    this.imageUrl = data?.imageUrl
    this.comment = data?.comment
    this.createdAt = data?.createdAt
    this.userName = data?.userName
    this.sent = data?.sent
  }
}

export default PhotoReview
