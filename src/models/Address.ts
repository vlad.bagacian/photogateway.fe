class Address {
  street: string;
  city: string;
  zipCode: string;

  constructor(data?: any
    
  ) {
    this.street = data?.street
    this.city = data?.city
    this.zipCode = data?.zipCode
  }
}

export default Address;