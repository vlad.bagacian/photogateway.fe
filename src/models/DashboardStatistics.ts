import SessionStatistics from "./SessionStatistics"

class DashboardStatistics {
  newProjects: number
  projectsInProgress: number
  completed: number
  totalUsers: number
  sessionStatistics: SessionStatistics[]
  constructor(data?: any) {
    this.newProjects = data?.newProjects
    this.projectsInProgress = data?.projectsInProgress
    this.completed = data?.completed
    this.totalUsers = data?.totalUsers
    this.sessionStatistics = data?.sessionStatistics
  }
}

export default DashboardStatistics
