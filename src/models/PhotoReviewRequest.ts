class PhotoReviewRequest {
  comment: string
  constructor(data?: string) {
    this.comment = data || ''
  }
}

export default PhotoReviewRequest
