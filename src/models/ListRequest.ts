export class ListRequest {
  top: number
  skip: number

  constructor(top?: number, skip?: number) {
    this.top = top ?? 100
    this.skip = skip ?? 0
  }

  public next() {
    this.skip += this.top
  }
}

export default ListRequest
