export class UpdateSessionRequest {
  title: string
  description: string
  cost: number
  constructor(data?: any) {
    this.cost = data?.cost ?? 0
    this.title = data?.title
    this.description = data?.description
  }
}

export default UpdateSessionRequest
