class ListResponse<T> {
  items: Array<T>
  count: number
  constructor(data?: any) {
    if (data === undefined || data === null) {
      this.items = new Array<T>()
      this.count = 0
    } else {
      this.items = data.items
      this.count = data.count
    }
  }
}

export default ListResponse
