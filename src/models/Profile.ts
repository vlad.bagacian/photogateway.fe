import Address from './Address'

class Profile {
  id: string
  userId: string
  email: string
  firstName: string
  lastName: string
  phone: string
  address: Address
  profileImageUrl: string
  role: string
  constructor(data: any) {
    this.id = data?.id
    this.userId = data?.userId
    this.email = data?.email
    this.firstName = data?.firstName
    this.lastName = data?.lastName
    this.phone = data?.phone
    this.address = data?.address
    this.profileImageUrl = data?.profileImageUrl
    this.role = data?.role
  }
}

export default Profile
