import SessionHistory from './SessionHistory'
import { SessionStatus } from '../constants/Common'

export class Session {
  startedAt?: string
  photographerId: string
  photographerName: string
  photographerAvatar: string
  photographerEmail: string
  clientId: string
  clientName: string
  clientAvatar: string
  clientEmail: string
  dueAt: string
  photoShootStartDate: string
  photoShootEndDate: string
  title: string
  description: string
  location: string
  cost: number
  status: SessionStatus
  id: string
  sessionHistory: SessionHistory[]
  constructor(data?: any) {
    this.startedAt = data?.startedAt
    this.photographerId = data?.photographerId
    this.photographerName = data?.photographerName
    this.photographerAvatar = data?.photographerAvatar
    this.photographerEmail = data?.photographerEmail
    this.clientId = data?.clientId
    this.clientName = data?.clientName
    this.clientAvatar = data?.clientAvatar
    this.clientEmail = data?.clientEmail
    this.dueAt = data?.dueAt
    this.photoShootStartDate = data?.photoShootStartDate
    this.photoShootEndDate = data?.photoShootEndDate
    this.title = data?.title
    this.description = data?.description
    this.location = data?.location
    this.cost = data?.cost
    this.status = data?.status
    this.id = data?.id
    this.sessionHistory = data?.sessionHistory
  }
}

export default Session
