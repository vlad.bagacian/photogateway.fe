class CreateSessionRequest {
  photographerId: string
  photoShootStartDate: string
  photoShootEndDate: string
  dueAt: string
  title: string
  location: string
  description: string
  photoCount: number
  constructor(data?: any) {
    this.photographerId = data?.photographerId
    this.photoShootStartDate =
      data?.photoShootStartDate
    this.photoShootEndDate =
      data?.photoShootEndDate
    this.dueAt = data?.dueAt
    this.title = data?.title
    this.description = data?.description
    this.photoCount = data?.photoCount
    this.location = data?.location
  }
}

export default CreateSessionRequest
