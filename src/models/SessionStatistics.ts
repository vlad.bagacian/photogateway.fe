class SessionStatistics {
  year: number
  month: number
  sessionCount: number
  totalCost: number
  constructor(data?: any) {
  this.year = data?.year;
  this.month = data?.month;
  this.sessionCount = data?.sessionCount;
  this.totalCost = data?.totalCost;
  }
}

export default SessionStatistics
