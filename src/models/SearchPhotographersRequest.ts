class SearchPhotographersRequest {
  location: string
  category: string
  startDate: string
  endDate: string
  costStartRange: number
  costEndRange: number
  sortBy: string
  sortDescending: boolean
  top: number
  skip: number
  constructor(data?: any) {
    this.location = data?.location
    this.category = data?.category
    this.startDate = data?.startDate ?? new Date().toJSON().slice(0, 10)
    this.endDate = data?.endDate ?? new Date().toJSON().slice(0, 10)
    this.costStartRange = data?.costStartRange ?? 0
    this.costEndRange = data?.costEndRange ?? 5000
    this.sortBy = data?.sortBy
    this.sortDescending = data?.sortDescending
    this.top = data?.top ?? 100
    this.skip = data?.skip ?? 0
  }
}

export default SearchPhotographersRequest
