import Profile from './Profile'
import PhotoMetadata from './PhotoMetadata'
class PhotographerProfile extends Profile {
  id: string
  biography: string
  skills: string[]
  cost: number
  rating: number
  instagramProfile: string
  facebookProfile: string
  isProfileCompleted: boolean
  portfolioPhotos: PhotoMetadata[]
  constructor(data?: any) {
    super(data)
    this.id = data?.id
    this.instagramProfile = data?.instagramProfile
    this.facebookProfile = data?.facebookProfile
    this.biography = data?.biography
    this.skills = data?.skills
    this.isProfileCompleted = data?.isProfileCompleted
    this.cost = data?.cost
    this.rating = data?.rating
    this.portfolioPhotos = data?.portfolioPhotos
  }
}

export default PhotographerProfile
