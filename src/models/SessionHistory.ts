export class SessionHistory {
  dateTime?: string
  status: string

  constructor(data?: any) {
    this.status = data?.status
    this.dateTime = data?.dateTime
  }
}

export default SessionHistory
