class PhotoMetadata {
  imageUrl: string
  ratio: number
  constructor(data?: any) {
    this.imageUrl = data?.imageUrl
    this.ratio = data?.facebratioook
  }
}

export default PhotoMetadata
