import PhotoMetadata from './PhotoMetadata'

export class Photography {
  id: string
  selected: boolean
  createdAt: string
  metadata: PhotoMetadata
  exposureSettings: {
    shutterSpeed: number
    aperture: number
    iso: number
  }
  cameraMake: string
  cameraModel: string

  constructor(data: any) {
    this.id = data.id
    this.selected = data.isSelected ?? data.selected ?? false
    this.createdAt = data.createdAt
    this.metadata = data.metadata
    this.exposureSettings = data.exposureSettings
    this.cameraMake = data.cameraMake
    this.cameraModel = data.cameraModel
  }
}

export default Photography
