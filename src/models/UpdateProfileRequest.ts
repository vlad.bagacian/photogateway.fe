class UpdateProfileRequest {
  biography: string
  skills: string[]
  cost: number
  facebookProfile: string
  instagramProfile: string
  constructor(data?: any) {
    this.biography = data?.biography
    this.skills = data?.skills
    this.cost = data?.cost ?? 50
    this.facebookProfile = data?.facebookProfile
    this.instagramProfile = data?.instagramProfile
  }
}

export default UpdateProfileRequest
