import { createStore } from 'vuex'
import { PhotographerProfile } from '../models'
import Profile from '../models/Profile'
import SearchPhotographersRequest from '../models/SearchPhotographersRequest'

export interface State {
  users: Record<string, PhotographerProfile | Profile>
  searchParams: SearchPhotographersRequest | null
  isLoggedIn: boolean
  profileId: string | null
}

export default createStore<State>({
  state: {
    users: {},
    isLoggedIn: false,
    searchParams: null,
    profileId: null
  },
  mutations: {
    setSearchParams(state, payload: SearchPhotographersRequest) {
      state.searchParams = payload
    },
    setUser(
      state,
      payload: { id: string; user: PhotographerProfile | Profile }
    ) {
      state.users[payload.id] = payload.user
    },
    setProfileId(state, payload: string) {
      state.profileId = payload
    }
  },
  actions: {
    // Add actions if needed, e.g., to fetch the user profile and save it using the setUser mutation
  },
  getters: {
    getUser: (state) => (id: string) => {
      return state.users[id]
    }
  }
})
