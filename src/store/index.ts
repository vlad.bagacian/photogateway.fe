import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import { getUserProfile } from '../api'
import { PhotographerProfile } from '../models'
import Profile from '../models/Profile'
import { ActionContext } from 'vuex'
import { AppSession } from '../constants/Common'

class AppSessionImp implements AppSession {
  constructor() {
    this.user = null
    this.isLoggedIn = false
    this.metadata = {}
  }
  user: PhotographerProfile | Profile | null
  isLoggedIn: boolean
  metadata: Record<string, object>
}

export default createStore<AppSession>({
  state: {
    user: null,
    isLoggedIn: false,
    metadata: {}
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload
    },
    setMetadata(state, payload) {
      state.metadata = payload
    },
    addToMap(state, payload: { key: string; value: object }) {
      state.metadata = { ...state.metadata, [payload.key]: payload.value }
    },
    clearState(state) {
       Object.assign(state, new AppSessionImp())
    }
  },
  getters: {
    getUser: (state) => (): PhotographerProfile | Profile | null => {
      return state.user
    },
    getMetadata: (state) => (): Record<string, object> => {
      return state.metadata
    },
    getValue: (state) =>
      (key: string): object | null => {
        return state.metadata[key] ?? null
      }
  },
  actions: {
    async loginUser(
      { commit, getters }: ActionContext<AppSession, unknown>,
      payload: { token: string }
    ) {
      const profile = await getUserProfile(payload.token)
      commit('setUser', profile)
    },
    addMetadata({ commit, getters }, payload) {
      commit('addToMap', payload)
    }
  },
  plugins: [createPersistedState()]
})
