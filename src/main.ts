// FILE: main.js

import { createApp } from 'vue'
import { Quasar } from 'quasar'
import router from './router'
import store from './store'
import { createAuth0, Auth0VueClientOptions } from '@auth0/auth0-vue'

// Import icon libraries
import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/material-icons/material-icons.css'
import '@mdi/font/css/materialdesignicons.css'

// Import Quasar css
import 'quasar/src/css/index.sass'

// Assumes your root component is App.vue
// and placed in same folder as main.js
import App from './App.vue'

const clientId = import.meta.env.VITE_AUTH_CLIENT_ID
const redirectUri = import.meta.env.VITE_AUTH_REDIRECT_URL
const audience = import.meta.env.VITE_AUTH_AUDIENCE
const photogatewayApp = createApp(App)

photogatewayApp.use(Quasar, {
  plugins: {}, // import Quasar plugins and add here
  config: {
    brand: {
      light: '#f5f5f5'
    }
  }
})

photogatewayApp.use(store)
photogatewayApp.use(router)

photogatewayApp.use(
  createAuth0({
    domain: 'photogateway.eu.auth0.com',
    clientId: clientId,
    authorizationParams: {
      redirect_uri: redirectUri,
      audience: audience
    }
  })
)

// Assumes you have a <div id="app"></div> in your index.html
photogatewayApp.mount('#app')
