import { createRouter, createWebHistory } from 'vue-router'
import { authGuard } from '@auth0/auth0-vue'
import Photographer from '../components/Photographer.vue'
import CompleteProfile from '../components/CompleteProfile.vue'
import PhotographerListView from '../components/PhotographerListView.vue'
import LogIn from '../components/LogIn.vue'
import Index from '../components/Index.vue'
import Session from '../components/Session.vue'
import Error from '../components/Error.vue'
import Register from '../views/Register.vue'
import UserProfile from '../views/UserProfile.vue'
import { PhotographyReview } from '../components'
const routes = [
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/photographers',
    name: 'photographers',
    component: PhotographerListView
  },
  {
    path: '/login',
    name: 'login',
    component: LogIn,
    beforeEnter: authGuard
  },
  {
    path: `/photographers/:photographerId`,
    name: 'photographer',
    component: Photographer
  },
  {
    path: '/sessions/:sessionId/photographies/:photographyId',
    name: 'photography',
    component: PhotographyReview
  },
  {
    path: '/callback',
    redirect: { path: '/' }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: UserProfile
  },
  {
    path: '/completeProfile',
    name: 'completeProfile',
    component: CompleteProfile,
    beforeEnter: authGuard
  },
  {
    path: '/sessions/:sessionId',
    name: 'sessions',
    component: Session,
    beforeEnter: authGuard
  },
  {
    path: '/',
    name: 'index',
    component: Index
  },
  {
    path: '/session',
    name: 'session',
    component: Session
  },
  {
    path: '/error/:errorCode',
    name: 'error',
    component: Error
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
