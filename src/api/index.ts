import {
  PhotographerProfile,
  Photography,
  ListResponse,
  UpdateProfileRequest,
  SearchPhotographersRequest,
  CreateUserRequest,
  Profile,
  Session,
  ListRequest,
  CreateSessionRequest,
  UpdateSessionRequest,
  PhotoReview,
  PhotoReviewRequest,
  PhotographerReviewRequest,
  PhotographerReviewResponse,
  SessionStatistics
} from '../models'
import DashboardStatistics from '../models/DashboardStatistics'

const apiUrl = import.meta.env.VITE_API_ENDPOINT

export const handleResponse = async (response: Response) => {
  if (!response.ok) {
    window.location.href = `/error/${response.status}`
  }

  return response
}

export const createUser = async (payload: CreateUserRequest): Promise<any> => {
  debugger
  const response = await fetch(`${apiUrl}/users`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  })
  await handleResponse(response)
}

export const getLocations = async (): Promise<string[]> => {
  const response = await fetch(`${apiUrl}/resources/locations`, {
    method: 'GET'
  })
  handleResponse(response)
  const data = await response.json()
  return data as string[]
}

export const getUserProfile = async (token: string): Promise<Profile> => {
  const response = await fetch(`${apiUrl}/users`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token
    }
  })
  handleResponse(response)
  const data = await response.json()
  return new Profile(data)
}

export const getMyAccount = async (
  token: string
): Promise<PhotographerProfile> => {
  const response = await fetch(`${apiUrl}/photographers/myaccount`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token
    }
  })
  handleResponse(response)
  const data = await response.json()
  return new PhotographerProfile(data)
}

export const getProfile = async (id: string): Promise<PhotographerProfile> => {
  const response = await fetch(`${apiUrl}/photographers/${id}`, {
    method: 'GET'
  })
  handleResponse(response)
  const data = await response.json()
  return new PhotographerProfile(data)
}

export const getPortfolio = async (
  albumId: string,
  token: string
): Promise<ListResponse<Photography>> => {
  const response = await fetch(`${apiUrl}/photographs/${albumId}`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token
    }
  })
  handleResponse(response)
  const data = await response.json()
  return new ListResponse<Photography>(data)
}

export const getSession = async (
  sessionId: string,
  token: string
): Promise<Session> => {
  const response = await fetch(`${apiUrl}/sessions/${sessionId}`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token
    }
  })

  handleResponse(response)
  const data = await response.json()
  return new Session(data)
}

export const createSession = async (
  request: CreateSessionRequest,
  token: string
): Promise<Session> => {
  const response = await fetch(`${apiUrl}/sessions`, {
    method: 'POST',
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(request)
  })
  handleResponse(response)
  const data = await response.json()
  return new Session(data)
}

export const getSessions = async (
  token: string,
  request: ListRequest
): Promise<ListResponse<Session>> => {
  const queryParams = `?top=${request.top}&skip=${request.skip}`
  const response = await fetch(`${apiUrl}/sessions${queryParams}`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token
    }
  })
  handleResponse(response)
  const data = await response.json()
  return new ListResponse<Session>(data)
}

export const moveBack = async (
  sessionId: string,
  token: string
): Promise<Session> => {
  const response = await fetch(`${apiUrl}/sessions/${sessionId}/back`, {
    method: 'POST',
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json'
    }
  })
  handleResponse(response)
  const data = await response.json()
  return new Session(data)
}

export const moveNext = async (
  sessionId: string,
  token: string
): Promise<Session> => {
  const response = await fetch(`${apiUrl}/sessions/${sessionId}/next`, {
    method: 'POST',
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json'
    }
  })
  handleResponse(response)
  const data = await response.json()
  return new Session(data)
}

export const updateProfile = async (
  request: UpdateProfileRequest,
  token: string
): Promise<any> => {
  const response = await fetch(`${apiUrl}/photographers/update-profile`, {
    method: 'POST',
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(request)
  })
  await handleResponse(response)
}

export const updateAvatar = async (
  avatar: FormData,
  token: string
): Promise<Profile> => {
  const response = await fetch(`${apiUrl}/users/update-avatar`, {
    method: 'POST',
    headers: {
      Authorization: 'Bearer ' + token
    },
    body: avatar
  })
  handleResponse(response)
  const data = await response.json()
  return new Profile(data)
}

export const updateSession = async (
  sessionId: string,
  request: UpdateSessionRequest,
  token: string
): Promise<any> => {
  const response = await fetch(`${apiUrl}/sessions/${sessionId}`, {
    method: 'PUT',
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(request)
  })
  await handleResponse(response)
}

export const updatePortfolio = async (
  files: FormData,
  token: string
): Promise<any> => {
  const response = await fetch(`${apiUrl}/photographers/update-portfolio`, {
    method: 'POST',
    headers: {
      Authorization: 'Bearer ' + token
    },
    body: files
  })
  await handleResponse(response)
}

export const addPhotosToSession = async (
  sessionId: string,
  files: FormData,
  token: string
): Promise<any> => {
  const response = await fetch(
    `${apiUrl}/sessions/${sessionId}/photographies`,
    {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token
      },
      body: files
    }
  )
  await handleResponse(response)
}

export const selectPhotosInSession = async (
  sessionId: string,
  photoId: string,
  token: string
): Promise<Photography> => {
  try {
    const response = await fetch(
      `${apiUrl}/sessions/${sessionId}/photographies/${photoId}/mark-selected`,
      {
        method: 'PUT',
        headers: {
          Authorization: 'Bearer ' + token
        }
      }
    )
    handleResponse(response)
    const data = await response.json()
    return new Photography(data)
  } catch (error) {
    throw error
  }
}

export const getPhotosInSession = async (
  sessionId: string,
  token: string
): Promise<ListResponse<Photography>> => {
  const response = await fetch(
    `${apiUrl}/sessions/${sessionId}/photographies`,
    {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }
  )
  handleResponse(response)
  const data = await response.json()
  return new ListResponse<Photography>(data)
}

export const getPhotoReviews = async (
  sessionId: string,
  photoId: string,
  token: string
): Promise<ListResponse<PhotoReview>> => {
  const response = await fetch(
    `${apiUrl}/sessions/${sessionId}/photographies/${photoId}/reviews`,
    {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }
  )
  handleResponse(response)
  const data = await response.json()
  return new ListResponse<PhotoReview>(data)
}

export const addPhotoReview = async (
  sessionId: string,
  photoId: string,
  request: PhotoReviewRequest,
  token: string
): Promise<any> => {
  try {
    const response = await fetch(
      `${apiUrl}/sessions/${sessionId}/photographies/${photoId}/reviews`,
      {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(request)
      }
    )
    handleResponse(response)
    const data = await response.json()
    return new ListResponse<PhotoReview>(data)
  } catch (error) {
    throw error
  }
}

export const updatePhoto = async (
  sessionId: string,
  photoId: string,
  files: FormData,
  token: string
): Promise<Photography> => {
  try {
    const response = await fetch(
      `${apiUrl}/sessions/${sessionId}/photographies/${photoId}`,
      {
        method: 'PUT',
        headers: {
          Authorization: 'Bearer ' + token
        },
        body: files
      }
    )
    handleResponse(response)
    const data = await response.json()
    return new Photography(data)
  } catch (error) {
    throw error
  }
}

export const downloadZip = async (
  sessionId: string,
  token: string
): Promise<Blob> => {
  const response = await fetch(
    `${apiUrl}/sessions/${sessionId}/photographies/zip`,
    {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      }
    }
  )
  if (!response.ok) {
    window.location.href = `/error/${response.status}`
  }
  return await response.blob()
}

export const searchPhotographers = async (
  request: SearchPhotographersRequest
): Promise<ListResponse<PhotographerProfile>> => {
  const response = await fetch(`${apiUrl}/photographers/search`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(request)
  })
  handleResponse(response)
  const data = await response.json()
  return new ListResponse<PhotographerProfile>(data)
}

export const addPhotographerReview = async (
  photographerId: string,
  request: PhotographerReviewRequest,
  token: string
): Promise<ListResponse<PhotographerReviewResponse>> => {
  try {
    const response = await fetch(
      `${apiUrl}/photographers/${photographerId}/reviews`,
      {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(request)
      }
    )
    handleResponse(response)
    const data = await response.json()
    return new ListResponse<PhotographerReviewResponse>(data)
  } catch (error) {
    throw error
  }
}

export const getPhotographerReview = async (
  photographerId: string,
  token: string
): Promise<ListResponse<PhotographerReviewResponse>> => {
  try {
    const response = await fetch(
      `${apiUrl}/photographers/${photographerId}/reviews`,
      {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + token
        }
      }
    )
    handleResponse(response)
    const data = await response.json()
    return new ListResponse<PhotographerReviewResponse>(data)
  } catch (error) {
    throw error
  }
}

export const getStatistics = async (
  token: string
): Promise<DashboardStatistics> => {
  try {
    const response = await fetch(`${apiUrl}/sessions/statistics`, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
    handleResponse(response)
    const data = await response.json()
    return new DashboardStatistics(data)
  } catch (error) {
    throw error
  }
}
