export const skillsMapping = new Map<string, string>([
  ['None', 'grey'],
  ['Landscape', 'orange'],
  ['Portrait', 'accent'],
  ['Wedding', 'red'],
  ['Fashion', 'blue'],
  ['Event', 'yellow'],
  ['Sports', 'green'],
  ['Aerial', 'accent'],
  ['Macro', 'blue'],
  ['Underwater', 'orange'],
  ['Wildlife', 'red'],
]);