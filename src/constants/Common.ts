export const format = (val: string | null | undefined) => {
  if (val === null || val == undefined) {
    return '-'
  }

  const date = new Date(val)
  const year = date.getFullYear()
  const month = `0${date.getMonth() + 1}`.slice(-2)
  const day = `0${date.getDate()}`.slice(-2)
  const hours = `0${date.getHours()}`.slice(-2)
  const minutes = `0${date.getMinutes()}`.slice(-2)
  const seconds = `0${date.getSeconds()}`.slice(-2)
  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
}

export class SessionMetadata {
  color: string
  icon: string
  title: string
  constructor(color: string, icon: string, title: string) {
    this.color = color
    this.icon = icon
    this.title = title
  }
}

export interface AppSession {
  user: PhotographerProfile | Profile | null
  isLoggedIn: boolean
  metadata: Record<string, object>
}

export enum SessionStatus {
  None = '',
  New = 'New',
  Cancelled = 'Cancelled',
  ProjectReview = 'ProjectReview',
  Approval = 'Approval',
  InProgress = 'InProgress',
  InReview = 'InReview',
  Completed = 'Completed'
}

export const getSessionStatusMapping = (status: string) => {
  switch (status) {
    case SessionStatus.New:
      return new SessionMetadata('grey', 'play_arrow', 'Project created')
    case SessionStatus.Cancelled:
      return new SessionMetadata('red', 'event_busy', 'Project cancelled')
    case SessionStatus.ProjectReview:
      return new SessionMetadata('green', 'check_circle', 'Project review')
    case SessionStatus.Approval:
      return new SessionMetadata('accent', 'check_circle', 'Project approved')
    case SessionStatus.InProgress:
      return new SessionMetadata(
        'blue',
        'photo_camera_back',
        'Photoshoot in progress'
      )
    case SessionStatus.InReview:
      return new SessionMetadata('orange', 'preview', 'Files sent for review')
    case SessionStatus.Completed:
      return new SessionMetadata('green', 'done_all', 'Project completed')
  }
}

export const statusMapping = new Map<string, string>([
  ['New', 'grey'],
  ['Cancelled', 'red'],
  ['Approval', 'accent'],
  ['InProgress', 'blue'],
  ['InReview', 'orange'],
  ['Reviewed', 'yellow'],
  ['Completed', 'green']
])

import { PhotographerProfile, PhotoMetadata, Profile } from '../models'

const apiUrl: string = import.meta.env.VITE_API_ENDPOINT

export const getImage = (
  photo: PhotoMetadata,
  width?: number,
  quality?: number
): string => {
  const baseUrl = `${apiUrl}/files/${photo.imageUrl}`
  const params = new URLSearchParams()

  if (width) {
    params.append('w', width.toString())
  }

  if (quality) {
    params.append('q', quality.toString())
  }

  const url = `${baseUrl}?${params.toString()}`
  return url
}

export const splitArrayIntoChunks = <T>(array: T[], chunks: number) => {
  const result = []
  const chunkSize =
    array.length < chunks ? 1 : Math.ceil(array.length / chunks)

  for (let i = 0; i < chunks; i++) {
    const startIndex = i * chunkSize
    const endIndex = startIndex + chunkSize
    const chunk = array.slice(startIndex, endIndex)
    result.push(chunk)
  }

  return result
}
